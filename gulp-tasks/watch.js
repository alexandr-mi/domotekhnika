module.exports = function (gulp, options, plugins) {

	return function (cb) {

		// STYLES
		gulp.watch([
				options.path.watch.sass, 
				'!src/assets/css/critical/critical.scss',
			], gulp.series(gulp.parallel('sass-styles', 'sass-wysywig', 'sass-component', 'sass-allComponents')))
		
		gulp.watch(['./src/components/**/*.{scss,sass}'], gulp.series(gulp.parallel('sass-component', 'sass-allComponents', 'sass-styles')))
		
		gulp.watch(['./src/**/critical.{scss,sass}'], gulp.series('sass-crit', 'pug'))

		// PUG
		global.watch = true;
		gulp.watch([options.path.watch.pug], gulp.series('pug-watch'))
			.on('all', function (event, filepath) {
				global.emittyChangedFile = filepath;
			});

		// JS
		gulp.watch('./src/**/*.js', gulp.series('scripts', 'scripts-components')).on("change", plugins.browserSync.reload);
		gulp.watch('./src/assets/js/vendor/**/*.js', gulp.series('scripts-libs')).on('change', plugins.browserSync.reload);

		// HTML
		gulp.watch(options.path.build.html + '*.html').on('change', plugins.browserSync.reload);

		// PICTURES
		gulp.watch(['./src/assets/images/*.{png,jpg,jpeg,gif,ico,svg,webp}'], gulp.series('imagemin'));
		gulp.watch([options.path.src.sprites + '**/*.svg'], gulp.series('svg'));

		// VIDEO
		gulp.watch(['./src/assets/video/*.*'], gulp.series('video'));

		cb();
	};

};
