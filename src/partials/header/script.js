(function() {
	const $DOCUMENT = $(document);

	const handler_search_clear_click = function() {
		const $search_clear = $(this);
		const $search_input = $search_clear.siblings(".js-header-search__input");

		console.log( $search_clear, $search_input );
		$search_input.val('');
		$search_input.focus();
	};

	$DOCUMENT.on('click', '.js-header-search__clear', handler_search_clear_click)
})();
