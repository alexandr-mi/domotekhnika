window.OPTS = {
	$DOCUMENT: $(document),
	$WINDOW: $(window),
	$BODY: $('body'),
	BODY: document.querySelector('body'),

	WW: $(window).width(),
}

window.SlamLight = {};

// import 'popper.js';
// import 'bootstrap';
import 'bootstrap/js/src/util.js';
import 'bootstrap/js/src/modal.js'; //модалки
import 'bootstrap/js/src/tab.js'; //табы
import 'bootstrap/js/src/collapse.js'; //аккордеоны

import uiInits from './init';


OPTS.$DOCUMENT.ready(() => {
	// inits
	uiInits.init();

	// vendor
	const btn_toggle = require('./vendor/btn-toggle');


	// Components
	const hero = require('../../components/hero/script');

	// Partials
	const header = require('../../partials/header/script');

	// Mixins
	const pages_viget = require('../../components/pages-viget/script.js');
	const lazy_img = require('../../mixins/lazy-img/script');
	const lazy_bg = require('../../mixins/lazy-bg/script');
	const lazy_video = require('../../mixins/lazy-video/script');
	const form = require('../../mixins/forms/script');
});

