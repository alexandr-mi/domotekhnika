(function() {

	$(document).on('click', '.js-form-control-clear', function() {
		const $clear = $(this);
		const $form_control_optional = $clear.closest('.js-form-control-optional');
		const $form_control = $form_control_optional.siblings(".js-form-control");
		$form_control.val('');
		$form_control.focus();
	})
})();
