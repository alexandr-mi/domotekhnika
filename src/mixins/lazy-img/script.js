(function() {
	var callback_loaded = function(el) {
		const $img = $(el)
		const $img_wrap = $img.closest('.lazy-img-wrap');

		$img_wrap.addClass('loaded')
	};

	new LazyLoad({
		elements_selector: ".lazy-img",
		callback_loaded: callback_loaded,
	});
})();
