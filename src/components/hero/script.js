(function () {
	const $hero_slider_arr = $('.js-hero-slider');
	$hero_slider_arr.each(function(i,el){
		const $hero_slider = $(el);

		$hero_slider.slick && $hero_slider.slick({
			dots: true,
			slidesToShow: 1,
			adaptiveHeight: true,
			prevArrow: '<button type="button" class="slick-prev"><svg class="icon"><use xlink:href="./images/sprite.svg#arrow"></use></svg></button>',
			nextArrow: '<button type="button" class="slick-next"><svg class="icon"><use xlink:href="./images/sprite.svg#arrow"></use></svg></button>',
			lazy_init_offset : 800,
			lazy_init : false,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						arrows: false
					}
				}
			]
		})
	});
})();
